# 35 SAWAYAMA WONDERDISC

This puzzle features a nice visualization of a copy protected disc drive.

Unlocking the disc drive was character wise, like dialing a phone number
via the modem. I do not tell where I got the key from.

We need to copy the original file and replace the region codes in the file
with our region code

For the copy we have a reader EXA and a writer EXA.

We send the data over the wire. 

We patch the data at the writer.

To distinguish numbers and keywords we make use of the observation that
all numbers in the files are non-negative.
So the test 

```assembler
TEST X > -1
```
can only fail if the `X` register contains a keyword.

Note that we later added the number -1 into the data flow to signal 
an end-of-file condition to the writer. 
So we first test for this special number.

For patching it would be nice to have three registers:
- one for the incoming keyword which is the original region code
- one for the new region code
- one to hold the test result

Here we solved this by storing the new region code at the start of
the file, accessing it when we need it by a

``` assembler
  SEEK -9999
  COPY F X
  SEEK 9999
```
sequence.

This means that we have to remove the region code at the start before we
drop the file onto the buffer host. Alas this is not that bad because we
can restore that code into the `X` register to be in start condition for
the next write cycle.