# 29 DIGITAL LIBRARY PROJECT

We got a list of book "call numbers" and have to retrieve copies of those
books. So we send our minions, one EXA for each book:

```assembler
GRAB 300

; SEND WORKERS
MARK SEND
COPY F X
REPL WORKER
JUMP SEND
```

The `X` register contains the book call number. So we first decode it:

```assembler 
MARK WORKER
LINK 800
; HOST TO T
SWIZ X 3 T
; FILENAME TO X
SWIZ X 21 X
ADDI X 200 X
```

So `512` decodes to host `5` and filename `12+200=212`.

We walk to the host:

```assembler
; WALK TO HOST T
MARK WALK
LINK 800
SUBI T 1 T
TJMP WALK
```

Then spawn a writer EXA and start reading and sending the book file data:

```assembler
; READER
REPL SPAWN2
GRAB X

MARK READ
COPY F M
JUMP READ
```

The writer just receives the data and stores it into a new file:

```assembler
; WRITER 
MARK SPAWN2
MAKE

MARK WRITE
TEST MRD
FJMP RETURN
COPY M F
JUMP WRITE
```

Communications is set to local mode to let each reader writer pair
communicate undisturbed. 

Writer uses `TEST MRD` to sense when reader has finished and 
terminated due to lack of data.

In this case the writer returns as far as possible until it
hits the root host where it terminates and leaves the copy.