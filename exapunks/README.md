# EXAPUNKS

This game was released by Zachtronics in 2018.

It features robot like machines called EXAs which can be programmed in
a simple assembler language.

EXAs can be spawned and killed and can communicate, so this game allows
for parallel programming.

## Links
- http://www.zachtronics.com/exapunks/
- https://en.wikipedia.org/wiki/Exapunks

